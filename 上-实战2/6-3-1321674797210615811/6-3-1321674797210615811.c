#include <stdio.h>

int any(char s1[], char s2[]);

int main()
{
    char s1[1001];
    char s2[1001];

    scanf("%s %s", s1, s2);
    printf("%d\n", any(s1, s2));

    return 0;
}


/* Your code will be copied here.*/
int any(char s1[], char s2[])
{
    char s[256];
    for(int i=0;i<256;s[i]=0,++i);
    for(int i=0;s2[i];s[s2[i]]=1,++i);
	for(int i=0;s1[i];++i)
		if(s[s1[i]])
			return i;
    return -1;
}