#include <stdio.h>

void display(unsigned x);
unsigned setbits(unsigned x, unsigned y, int p, int n);

int main()
{
    unsigned x, y;
    int p, n;
    display(setbits(1,8,10,10));
    while(scanf("%u%u%d%d", &x, &y, &p, &n) != EOF){
        unsigned ans = setbits(x, y, p, n);
        display(ans);
    }

    return 0;
}

void display(unsigned x)
{
    for(int i = 31; i >= 0; i--){
        printf("%d", (x>>i)&1);
    }
    putchar('\n');
}


/* Your code will be copied here. */
// unsigned setbits(unsigned x, unsigned y, int p, int n)
// {
	// if(n==32)return y;
    // return x & ~(~(~0<<n)<<(p+1-n))|((y&(~(~0<<n)))<<(p+1-n));
// }
unsigned setbits(unsigned x, unsigned y, int p, int n)
{
    return x & ~(~(~0ll<<n)<<(p+1-n))|((y&(~(~0ll<<n)))<<(p+1-n));
}