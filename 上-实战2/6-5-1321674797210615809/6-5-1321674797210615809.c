#include <stdio.h>

void display(unsigned x);
unsigned invert(unsigned x, int pos, int n);
unsigned invert2(unsigned x, int pos, int n);
unsigned invert3(unsigned x, int pos, int n);

int main()
{
    unsigned x;
    int pos, n;

    while(scanf("%u%d%d", &x, &pos, &n) != EOF){
        display(x);
        display(invert(x, pos, n));
        display(invert2(x, pos, n));
        display(invert3(x, pos, n));
    }

    return 0;
}

void display(unsigned x)
{
    for(int i = 31; i >= 0; i--){
        printf("%d",(x>>i)&1);
    }
    putchar('\n');
}
/* Your code will be copied here*/
unsigned invert(unsigned x, int pos, int n)
{
    return x^(~(~0<<n)<<(pos+1-n));;
}
unsigned invert2(unsigned x, int pos, int n)
{
    return (x&(~(~((~0)<<n)<<(pos+1-n))))|((x>>(pos+1-n)^((~((~0)<<n)))<<(pos+1-n)));
}
unsigned invert3(unsigned x, int pos, int n)
{
    for(int i=pos-n+1;i<=pos;++i)
    {
        unsigned bit=(x>>i)&1;
        bit=(!bit)&1;
        x&=~(1<<i);
        x|=(bit<<i);
    }
    return x;
}
