#include <stdio.h>

void display(unsigned x);
unsigned rightrot(unsigned x, int n);

int main()
{
//    freopen("0.in", "r", stdin);
//    freopen("0.out", "w", stdout);
    unsigned x;
    int n;

    while(scanf("%u%d", &x, &n) != EOF) {
        display( rightrot(x, n) );
    }

    return 0;
}

void display(unsigned x)
{
    for(int i = 31; i >= 0; i--){
        printf("%d",(x>>i)&1);
    }
    putchar('\n');
}

/* Your code will be copied here. */
unsigned rightrot(unsigned x, int n){return (x<<(32-n))|(x>>n);}
