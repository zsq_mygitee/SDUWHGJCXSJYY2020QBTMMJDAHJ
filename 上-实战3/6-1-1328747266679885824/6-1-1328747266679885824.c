#include <stdio.h>
#include <stdlib.h>

int binsearch_naive(int x, int v[ ], int n);
int compare (const void * a, const void * b);

int main ()
{
    int n; //数组大小
    int m; //询问的数量

    scanf("%d", &n);

    int * v = (int*)malloc(sizeof(int) * n);

    for(int i = 0; i < n; i++) {
        scanf("%d", &v[i]);
    }

    qsort(v, n, sizeof(int), compare); //排序

    scanf("%d", &m);

    while(m--) {
        int x;
        scanf("%d", &x);
        printf("%d\n", binsearch_naive(x, v, n));
    }

    free(v);

    return 0;
}

int compare (const void * a, const void * b)
{
  return ( *(int*)a - *(int*)b );
}

/* 请在这里填写答案 */
int binsearch_naive(int x, int v[], int n)
{
    for(int l=0,r=n-1,m=0;l<=r;)
        if(v[m=((l+r)>>1)]==x)
            return m;
        else if(v[m]>x)
            r=m-1;
        else 
            l=m+1;
    return -1;
}