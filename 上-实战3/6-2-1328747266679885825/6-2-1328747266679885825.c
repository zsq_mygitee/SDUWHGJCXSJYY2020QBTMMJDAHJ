#include <stdio.h>
#include <assert.h>
//using namespace std;
//constexpr int N = 100001;
const int N = 100001;

void escape(char [], char []);
int getline(char []);

int main()
{
    char s[N * 2], t[N];

    while(getline(t) != EOF) {
        escape(s, t);
        printf("%s\n", s);
    }

    return 0;
}

int getline(char t[])
{
    int l = 0;
    char c;

    while(l + 2 <= N && (c = getchar()) != '\n' && c != EOF)
        t[l++] = c;

    if(c == EOF) return EOF;

    assert(l + 2 <= N);
    t[l++] = '\n';
    t[l++] = '\0';

    return l;
}


/* 请在这里填写答案 */
void escape(char *t,char *s)
{
    for(;*s;++s)
        if(*s=='\t')      *(t++)='\\',*(t++)='t';
        else if(*s=='\n') *(t++)='\\',*(t++)='n';
        else              *(t++)=*s;
}
