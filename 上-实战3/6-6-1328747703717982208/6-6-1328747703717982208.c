#include <stdio.h>

int nas( int a, int n );
int Sumofas( int a, int n );

int main()
{
    int a, n;

    scanf("%d %d", &a, &n);
    printf("nas(%d, %d) = %d\n", a, n, nas(a,n));        
    printf("s = %d\n", Sumofas(a,n));    

    return 0;
}
/* Your code is placed here */
int nas( int a, int n )
{
    int ans=0;
    while(n--)ans=(ans<<3)+(ans<<1)+a;
    return ans;
}
int Sumofas( int a, int n )
{
    int ans=0;
    for(int i=1;i<=n;ans+=nas(a,i),++i);
    return ans;
}