#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define R register
#define L long
#define LL long long
#define I inline
#define U unsigned

I LL read()
{
	R LL x;R char f;R char c;
	for (f=0; (c=getchar())<'0'||c>'9'; f=c=='-');
	for (x=c-'0'; (c=getchar())>='0'&&c<='9'; x=(x<<3)+(x<<1)+c-'0');
	return f?-x:x;
}
char pai[][10]={"",
           "S1","S2","S3","S4","S5","S6","S7","S8","S9","S10","S11","S12","S13",
           "H1","H2","H3","H4","H5","H6","H7","H8","H9","H10","H11","H12","H13",
           "C1","C2","C3","C4","C5","C6","C7","C8","C9","C10","C11","C12","C13",
           "D1","D2","D3","D4","D5","D6","D7","D8","D9","D10","D11","D12","D13",
           "J1","J2"};
int main()
{
    int k=read();
 	int data[2][60],shu[60];
    for(int i=1;i<=54;data[0][i]=i,shu[i]=read(),++i);
    for(int i=0;i<k;++i)
        for(int j=1;j<=54;++j)
            data[i&1?0:1][shu[j]]=data[i&1?1:0][j];
    for(int j=1;j<=54;++j)
        printf("%s%s",j==1?"":" ",pai[data[k&1?1:0][j]]);
    putchar('\n');
    return 0;
}
