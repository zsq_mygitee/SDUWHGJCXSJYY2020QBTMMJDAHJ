#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define R register
#define L long
#define LL long long
#define I inline
#define U unsigned
LL gcd(LL a,LL b){return b==0?a:gcd(b,a%b);};
LL aabs(LL a){return a>0?a:-a;}
I LL read()
{
	R LL x;R char f;R char c;
	for (f=0; (c=getchar())<'0'||c>'9'; f=c=='-');
	for (x=c-'0'; (c=getchar())>='0'&&c<='9'; x=(x<<3)+(x<<1)+c-'0');
	return f?-x:x;
}




int main()
{
    int n=read();
	LL a_zi,a_mu;
    if(n==0)printf("0\n");
    else
    {
        a_zi=read(),a_mu=read();
        LL g;
        for(int i=1;i<n;++i)
        {
            LL zi=read(),mu=read();
            g=gcd(a_mu,mu);
            a_zi=mu/g*a_zi+a_mu/g*zi;
            a_mu=a_mu/g*mu;
            g=gcd(aabs(a_zi),a_mu),a_zi/=g,a_mu/=g;
        }        
        g=gcd(aabs(a_zi),a_mu),a_zi/=g,a_mu/=g;
        if(a_zi==0)	            printf("0\n");
        else if(aabs(a_zi)<a_mu)printf("%lld/%lld\n",a_zi,a_mu);
        else if(aabs(a_zi)%a_mu)printf("%lld %lld/%lld\n",a_zi/a_mu,aabs(a_zi)%a_mu,a_mu);
        else				    printf("%lld\n",a_zi/a_mu);        
    }
 	return 0;
}
