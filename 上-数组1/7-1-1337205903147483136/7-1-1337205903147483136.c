#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define R register
#define L long
#define LL long long
#define I inline
#define U unsigned

I LL read()
{
	R LL x;R char f;R char c;
	for (f=0; (c=getchar())<'0'||c>'9'; f=c=='-');
	for (x=c-'0'; (c=getchar())>='0'&&c<='9'; x=(x<<3)+(x<<1)+c-'0');
	return f?-x:x;
}

#define MAXN 11
int a[MAXN];

int main()
{
    int n=read();
    for(int i=0;i<n;a[i]=read(),++i);
    int x=read(),i;
    for(i=n;i>0&&a[i-1]>x;--i)a[i]=a[i-1];
    a[i]=x;
    for(int i=0;i<n+1;printf("%d ",a[i]),++i);
    
    
 	return 0;
}
