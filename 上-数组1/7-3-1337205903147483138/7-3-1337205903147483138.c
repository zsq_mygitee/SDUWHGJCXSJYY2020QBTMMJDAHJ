#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define R register
#define L long
#define LL long long
#define I inline
#define U unsigned

I LL read()
{
	R LL x;R char f;R char c;
	for (f=0; (c=getchar())<'0'||c>'9'; f=c=='-');
	for (x=c-'0'; (c=getchar())>='0'&&c<='9'; x=(x<<3)+(x<<1)+c-'0');
	return f?-x:x;
}

#define MAXN 11
int a[MAXN];

int main()
{
    int n=read();
    for(int i=0;i<n;a[i]=read(),++i);
    int minni=0,maxxi=0;
    for(int i=1;i<n;++i)
        if(a[i]<a[minni])minni=i;
    int tmp=a[minni];
    a[minni]=a[0];
    a[0]=tmp;
    
    for(int i=1;i<n;++i)
        if(a[i]>a[maxxi])maxxi=i;
    
    tmp=a[maxxi];
    a[maxxi]=a[n-1];
    a[n-1]=tmp;



    for(int i=0;i<n;printf("%d ",a[i]),++i);
    
    
 	return 0;
}
