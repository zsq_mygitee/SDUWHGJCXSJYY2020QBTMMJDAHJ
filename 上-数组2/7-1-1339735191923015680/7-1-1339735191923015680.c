#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define R register
#define L long
#define LL long long
#define I inline
#define U unsigned

I LL read()
{
	R LL x;R char f;R char c;
	for (f=0; (c=getchar())<'0'||c>'9'; f=c=='-');
	for (x=c-'0'; (c=getchar())>='0'&&c<='9'; x=(x<<3)+(x<<1)+c-'0');
	return f?-x:x;
}



int a[100][100];
int main()
{
    int n=read();
    for(int i=0;i<n;++i)for(int j=0;j<n;++j)a[i][j]=read();
    int ans=0;
    for(int i=0;i<n-1;++i)
        for(int j=0;j<n-1;++j)
            if(n-1-i!=j)
                ans+=a[i][j];
    printf("%d",ans);
    
 	return 0;
}
