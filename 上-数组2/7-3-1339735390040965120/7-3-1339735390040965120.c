#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define R register
#define L long
#define LL long long
#define I inline
#define U unsigned

I LL read()
{
	R LL x;R char f;R char c;
	for (f=0; (c=getchar())<'0'||c>'9'; f=c=='-');
	for (x=c-'0'; (c=getchar())>='0'&&c<='9'; x=(x<<3)+(x<<1)+c-'0');
	return f?-x:x;
}



int a[100][100];
int main()
{
    int n=read();
    a[1][1]=1;
    for(int i=2;i<=n;++i)
        for(int j=1;j<=i;++j)
            a[i][j]=a[i-1][j-1]+a[i-1][j];
    
    for(int i=1;i<=n;++i)
    {
        for(int j=0;j<n-i;++j,putchar(' '));
        for(int j=1;j<=i;++j)
            printf("%4d",a[i][j]);
        putchar('\n');
    }
 	return 0;
}
