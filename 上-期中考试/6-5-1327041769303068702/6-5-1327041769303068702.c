int power(int base, int n)
{
	int ans=1;
	for(int i=0;i<n;++i,ans*=base);	
	return ans;
}