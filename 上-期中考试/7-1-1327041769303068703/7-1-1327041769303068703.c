#include <stdio.h>
int main()
{
	int n=0;
	scanf("%d",&n);
	if(n==0)
		printf("No need to print.");
	else
	{
		printf("Print \"hello, world!\" %d time(s) :\n",n);
		while(n--)printf("hello, world!\n");
	}
	return 0;
}