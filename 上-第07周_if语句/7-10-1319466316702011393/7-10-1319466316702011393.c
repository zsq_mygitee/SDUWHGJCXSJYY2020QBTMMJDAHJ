#include <stdio.h>
int pow(int n,int m)
{
	int ans=1;
	while(--m)
		ans*=n;
	return ans;
}
int narcissistic( int number )
{
	int l=0,a=number;
	int num[10]={0};
	int ans=0;
	while(number)
	{
		num[l]=number%10;
		number/=10;
		++l;
	}
	for(int i=0;i<l;++i)
		ans+=pow(num[i],l+1);
	return ans==a;
}
int main()
{
	int a;
	scanf("%d",&a);
	if(a<100||a>999)printf("Invalid Value.");
	else if(narcissistic(a))printf("Yes");
	else printf("No");
    return 0;
}
