#include <stdio.h>

#define MAXLINE 1000
char line[MAXLINE + 1];

int strlen_naive(const char s[]);

/* the length of string s*/
int main(){
    char ch[] = "hello world!";

    printf("The size of type char = %d\n", sizeof(char));
    printf("The size of char-array ch = %d\n", sizeof ch);
    printf("The size of char-array ch = %d\n", strlen_naive(ch));
    printf("The size of char-array line = %d\n", sizeof line);

    return 0;
}
/* 请在这里填写答案 */
int strlen_naive(const char s[])
{
	int n=0;
	while(s[n])++n;
	return n;
}
