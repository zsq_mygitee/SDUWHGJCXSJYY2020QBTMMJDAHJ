/* atoi: convert s to integer */
#include <stdio.h>

int atoi_naive( const char s[]);

int main()
{
    char s[8];
	
    scanf("%s", s);
		
    printf("%d\n", atoi_naive(s));

    return 0;
}
/* 请在这里填写答案 */
#include <stdlib.h>
int atoi_naive( const char s[]){return atoi(s);}
