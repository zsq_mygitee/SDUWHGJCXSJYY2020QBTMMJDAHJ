#include <stdio.h>

void squeeze(char s[], int c);

/* the length of a string */
int main(){
    char s[100];
    char c;
    int i;

    scanf("%s %c", s, &c);

    squeeze(s, c);
    for (i = 0; s[i] != '\0'; i++)
        putchar(s[i]);
    putchar('\n');

    return 0;
}
/* 请在这里填写答案 */
void squeeze(char s[], int c)
{
	int i=0,j=0;
	for(;s[i];++i)
		if(s[i]!=c)
			s[j++]=s[i];
	s[j]=0;
}