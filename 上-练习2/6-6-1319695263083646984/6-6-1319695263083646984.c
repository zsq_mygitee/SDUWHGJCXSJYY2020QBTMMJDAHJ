#include <stdio.h>

void strcat(char s[], char t[]);

/* the length of a string */
int main(){
    char s[100], t[10];
    int i;

    scanf("%s%s", s, t);

    strcat(s, t);
    for (i = 0; s[i] != '\0'; i++)
        putchar(s[i]);
    putchar('\n');

    return 0;
}

/* 请在这里填写答案 */
void strcat(char s[], char t[])
{
	int l=0;
	while(s[l])++l;
	int l2=0;
	while(t[l2])s[l]=t[l2],++l,++l2;
}