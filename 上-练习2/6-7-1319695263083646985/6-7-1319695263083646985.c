#include <stdio.h>

int lower(int c);

int main()
{
    char c;

    c = getchar();

    printf("%c", lower(c));

    return 0;
}
/* 请在这里填写答案 */
int lower(int c)
{
	return (c>='A'&&c<='Z')?c-'A'+'a':c;
	
}
