#include <stdio.h>

int bitcount(unsigned x);

int main()
{
    unsigned x;

    while(scanf("%u", &x) != EOF) {
        printf("%d\n", bitcount(x));
    }

    return 0;
}

/* 请在这里填写答案 */
int bitcount(unsigned x)
{
    int ans=0;
    for(int i=0;i<32;++i)
        ans+=(x&1),x>>=1;
    return ans;
}