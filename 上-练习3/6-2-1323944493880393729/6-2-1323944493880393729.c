#include<stdio.h>
const int LEN = 1001;

int atoi(char s[]);
int getline(char s[], int maxLine);

int main()
{
    printf("The integer value of \"%s\" is %d.\n", "123", atoi("123"));
    printf("The integer value of \"%s\" is %d.\n", "  +123", atoi("  +123"));
    printf("The integer value of \"%s\" is %d.\n", "    -123", atoi("    -123"));
    printf("The integer value of \"%s\" is %d.\n", "  -12 3", atoi("  -12 3"));

    char s[LEN];

    while(getline(s, LEN)) {
        printf("%d\n", atoi(s));
    }

    return 0;
}

int getline(char s[], int len)
{
    int i = 0;
    while(i + 1 < len && (s[i] = getchar()) != EOF && s[i] != '\n')
        ++i;

    if(s[i] == EOF) return 0;

    s[i] = '\0';

    return 1;
}


/* 请在这里填写答案 */
int atoi(char s[])
{
    int x=0;char f=0;int i;
    for(i=0;s[i]&&(s[i]<'0'||s[i]>'9');f=(s[i]=='-'),++i);
    for(;s[i]>='0'&&s[i]<='9';x=(x<<3)+(x<<1)+s[i]-'0',++i);
    return f?-x:x;
}