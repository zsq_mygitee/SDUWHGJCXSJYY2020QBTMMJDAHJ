#include<stdio.h>
#include <string.h>

void reverse(char s[]);
char s[1000001];

int main()
{
    while(~scanf("%s", s)) {
        reverse(s);
        printf("%s\n", s);
    }

    return 0;
}

/* 请在这里填写答案 */
void reverse(char s[])
{
    int l=0;while(s[l])++l;
    for(int i=0,n=l>>1;i<n;++i)
    {
        char c=s[l-i-1];
        s[l-i-1]=s[i];
        s[i]=c;
    }
    
}