#include <stdio.h>
int main()
{
    char c;
    int ans[10]={0};
    int space=0;//最后一个\n
    int other=0;
    while((c=getchar())!=EOF)
    {
        switch(c)
        {
            case '0':++ans[0];break;
            case '1':++ans[1];break;
            case '2':++ans[2];break;
            case '3':++ans[3];break;
            case '4':++ans[4];break;
            case '5':++ans[5];break;
            case '6':++ans[6];break;
            case '7':++ans[7];break;
            case '8':++ans[8];break;
            case '9':++ans[9];break;
            case ' ':
            case '\n':
            case '\t':
                ++space;
                break;
            default:
                ++other;
        }
    }
    printf("digits = %d %d %d %d %d %d %d %d %d %d, white space = %d, other = %d\n",ans[0],ans[1],ans[2],ans[3],ans[4],ans[5],ans[6],ans[7],ans[8],ans[9],space,other);
}