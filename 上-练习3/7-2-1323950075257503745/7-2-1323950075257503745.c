#include <stdio.h>
int main()
{
    char c;
    int lines=0;
    int words=0;
    int characters=0;
    char out_word_flag=1;
    while((c=getchar())!=EOF)
    {
        ++characters;
        if(c=='\n')++lines;
        if(c=='\n'||c==' '||c=='\t')out_word_flag=1;
        else if(out_word_flag)++words,out_word_flag=0;
    }
    printf("%d %d %d\n",lines,words,characters);
}