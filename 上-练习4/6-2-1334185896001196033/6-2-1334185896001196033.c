#include <stdio.h>
#include <string.h>

#define MAXLINE 1000 /* maximum input line length */

int getline_my(char line[], int max);
int strindex(char source[], char searchfor[]);

/* find all lines matching pattern */
int main()
{
    char line[MAXLINE];
    char pattern[5]; /* pattern to search for */
    int found = 0;

    getline_my(pattern, 5);
    int len = strlen(pattern);
    pattern[len - 1] = '\0';

    while (getline_my(line, MAXLINE) > 0) {
        int idx;
        if ((idx = strindex(line, pattern)) >= 0) {
            printf("%s", line);
            printf("idx %d\n", idx);
            found++;
        }
    }


    return found;
}

/* getline: get line into s, return length */
int getline_my(char s[], int lim)
{
    int c, i;

    i = 0;
    while (--lim > 0 && (c = getchar()) != EOF && c != '\n')
        s[i++] = c;

    if (c == '\n')
        s[i++] = c;

    s[i] = '\0';
    return i;
}
/* 请在这里填写答案 */
int strindex(char source[], char searchfor[])
{
    for(int i=0,j;source[i]!='\n'&&source[i];++i)
    {
        for(j=0;source[i]!='\n'&&source[i]&&searchfor[j]==source[i];++j,++i);
        if(!searchfor[j])
            return i-j;
    }
    return -1;
}