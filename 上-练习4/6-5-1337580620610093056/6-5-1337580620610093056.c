#include<stdio.h>

void qsort(int v[], int left, int right);

int main()
{
    int n;
    int * v = NULL;

    while(~scanf("%d", &n)) {
        v = (int *)malloc(n * sizeof(int));

        for(int i = 0; i < n; i++) {
            scanf("%d", i + v);
        }

        qsort(v, 0, n - 1);

        for(int i = 0; i < n - 1; i++) {
            printf("%d ", v[i]);
        }
        printf("%d\n", v[n - 1]);

        free(v);
    }

    return 0;
}

/* 请在这里填写答案 */
void swap(int v[],int i,int j)
{
    int tmp=v[i];
    v[i]=v[j];
    v[j]=tmp;
}
void qsort(int v[], int left, int right)
{
    if(left>=right)
        return;
    swap(v,left,(left+right)>>1);
    int last=left;
    for(int i=left+1;i<=right;++i)
        if(v[i]<v[left])
            swap(v,++last,i);
    swap(v,left,last);
    qsort(v,left,last-1);
    qsort(v,last+1,right);
}