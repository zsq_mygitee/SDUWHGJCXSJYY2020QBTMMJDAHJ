#include<stdio.h>

void strcpy( char *restrict dest, const char *restrict src );
char *alloc(int n);

int main()
{
    char *from = alloc(1000);
    char *to = alloc(1000);

    while(~scanf("%s", from)) {
        strcpy(to, from);
        printf("%s\n", to);
    }

    return 0;
}

/* 请在这里填写答案 */
void strcpy( char *restrict dest, const char *restrict src )
{
    while(*src)*dest=*src,++dest,++src;
    *dest=0;
}