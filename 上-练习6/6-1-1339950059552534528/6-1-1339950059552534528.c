#include <stdio.h>

int day_of_year(int year, int month, int day);
void month_day(int year, int yearday, int *pmonth, int *pday);

int main()
{
 //   freopen("0.in", "r", stdin);
 //   freopen("0.out", "w", stdout);

    printf("January 1 is the day %d of year 2020.\n", day_of_year(2020, 1, 1));
    printf("March 1 is the day %d of year 2020.\n", day_of_year(2020, 3, 1));
    printf("March 1 is the day %d of year 2019.\n\n", day_of_year(2019, 3, 1));

    int year;
    int month;
    int day;

    int tmp;
    scanf("%d", &tmp);

    while(tmp--) {
        scanf("%d%d%d", &year, &month, &day);
        printf("%d\n", day_of_year(year, month, day));
    }

    month_day(2020, 1, &month, &day);
    printf("The day 1 of year 2020 is %d/%d.\n", month, day);
    month_day(2020, 61, &month, &day);
    printf("The day 61 of year 2020 is %d/%d.\n", month, day);
    month_day(2019, 60, &month, &day);
    printf("The day 60 of year 2019 is %d/%d.\n", month, day);


    while(scanf("%d%d", &year, &tmp) == 2) {
        month_day(year, tmp, &month, &day);
        printf("%d %d\n", month, day);
    }

    return 0;
}

static char daytab[2][13] = {
    {0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31},
    {0, 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31}
};
/* 请在这里填写答案 */
char isrun(int year){return (year%4==0&&year%100!=0)||(year%400==0);}
int day_of_year(int year, int month, int day)
{
    char run = isrun(year);
    while(--month)
        day+=daytab[run][month];
    
    return day;
}
void month_day(int year, int yearday, int *pmonth, int *pday)
{
    char run = isrun(year);
    for(int i=1;i<=12;++i)
    {
        if(yearday-daytab[run][i]>0)
            yearday-=daytab[run][i];
        else
            {*pday=yearday,*pmonth=i;return;}
    }
}
