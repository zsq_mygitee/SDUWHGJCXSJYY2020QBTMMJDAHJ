#include <stdio.h>

char *month_name(int n);

int main()
{
    printf("The month 1 is %s.\n", month_name(1));
    printf("The month 13 is %s.\n", month_name(13));

    int month;

    while(scanf("%d", &month) == 1) {
        printf("%s\n", month_name(month));
    }

    return 0;
}

/* 请在这里填写答案 */

char month[][15]={"Illegal month","January","February","March","April","May","June","July","August","September","October","November","December"};
char *month_name(int n)
{
    return month[n<0?0:n>12?0:n];
}