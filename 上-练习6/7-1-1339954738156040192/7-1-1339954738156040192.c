#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define R register
#define L long
#define LL long long
#define I inline
#define U unsigned

I LL read()
{
	R LL x;R char f;R char c;
	for (f=0; (c=getchar())<'0'||c>'9'; f=c=='-');
	for (x=c-'0'; (c=getchar())>='0'&&c<='9'; x=(x<<3)+(x<<1)+c-'0');
	return f?-x:x;
}




int main()
{
    int h=read(),m=read(),s=read(),n=read();
    s+=n;
    m+=s/60;
    s%=60;
    h+=m/60;
    m%=60;
    h%=24;
    printf("%02d:%02d:%02d",h,m,s);
 	return 0;
}
