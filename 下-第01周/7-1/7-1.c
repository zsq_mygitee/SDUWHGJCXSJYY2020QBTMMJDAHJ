#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define R register
#define L long
#define LL long long
#define I inline
#define U unsigned

I LL read()
{
	R LL x;R char f;R char c;
	for (f=0; (c=getchar())<'0'||c>'9'; f=c=='-');
	for (x=c-'0'; (c=getchar())>='0'&&c<='9'; x=(x<<3)+(x<<1)+c-'0');
	return f?-x:x;
}
#define MAXN 11111
int    type[5];
#define max(x,y)    ((x)>(y)?(x):(y))
#define min(x,y)    ((x)<(y)?(x):(y))
void print_tiaozi(double n)
{
    for(int i=0,nn=n+0.5;i<nn;putchar('*'),++i);
}
int main()
{
    int n=read();
    double maxx=0,minn=10000,sum=0;
    for(int i=0;i<n;++i)
    {
        double tmp=0;
        scanf("%lf",&tmp);
        sum+=tmp;
        maxx=max(maxx,tmp);
        minn=min(minn,tmp);
        printf("%03d:%6.1f ",i+1,tmp);
        print_tiaozi(tmp);
        putchar('\n');
        if(tmp>=90)     ++type[0];
        else if(tmp>=80)++type[1];
        else if(tmp>=70)++type[2];
        else if(tmp>=60)++type[3];
        else            ++type[4];
    }
    putchar('\n');
    printf("Max:%6.1f ",maxx) ;print_tiaozi(maxx) ;putchar('\n');
    printf("Min:%6.1f ",minn) ;print_tiaozi(minn) ;putchar('\n');
    printf("Avg:%6.1f ",sum/n);print_tiaozi(sum/n);putchar('\n');
    putchar('\n');
    printf("A:%6.1f%% ",100.0*type[0]/n);print_tiaozi(100.0*type[0]/n);putchar('\n');
    printf("B:%6.1f%% ",100.0*type[1]/n);print_tiaozi(100.0*type[1]/n);putchar('\n');
    printf("C:%6.1f%% ",100.0*type[2]/n);print_tiaozi(100.0*type[2]/n);putchar('\n');
    printf("D:%6.1f%% ",100.0*type[3]/n);print_tiaozi(100.0*type[3]/n);putchar('\n');
    printf("E:%6.1f%% ",100.0*type[4]/n);print_tiaozi(100.0*type[4]/n);
    
 	return 0;
}
