#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define R register
#define L long
#define LL long long
#define I inline
#define U unsigned

I LL read()
{
	R LL x;R char f;R char c;
	for (f=0; (c=getchar())<'0'||c>'9'; f=c=='-');
	for (x=c-'0'; (c=getchar())>='0'&&c<='9'; x=(x<<3)+(x<<1)+c-'0');
	return f?-x:x;
}




int main()
{
    int n,m;
    scanf("%d%d",&n,&m);
    for(int i=1;i<=m;++i)
    {
        int k;
        scanf("%d",&k);
        if(k<0)             break;
        else if(k==n)
        {
            if(i==1)        printf("Bingo!\n");
            else if(i<=3)   printf("Lucky You!\n");
            else            printf("Good Guess!\n");
            goto exit;
        }
        else if(k>n)        printf("Too big\n");
        else if(k<n)        printf("Too small\n");
    }
    printf("Game Over\n");
exit:;
 	return 0;
}
