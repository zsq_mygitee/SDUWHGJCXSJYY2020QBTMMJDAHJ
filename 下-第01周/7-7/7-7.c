#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#define R register
#define L long
#define LL long long
#define I inline
#define U unsigned

I LL read()
{
	R LL x;R char f;R char c;
	for (f=0; (c=getchar())<'0'||c>'9'; f=c=='-');
	for (x=c-'0'; (c=getchar())>='0'&&c<='9'; x=(x<<3)+(x<<1)+c-'0');
	return f?-x:x;
}
int prime(int n)
{
    if(n<2)
        return 0;
    for(int i=2;i<=sqrt(n);++i)
        if(n%i==0)
            return 0;
    return 1;
}



int main()
{
    for(int i=1,n=read();i<=n;++i)
        if(prime(i)&&prime(n-i))
        {
            printf("%d = %d + %d",n,i,n-i);
            break;
        }
 	return 0;
}
