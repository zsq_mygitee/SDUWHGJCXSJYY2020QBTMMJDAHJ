#include <iostream>
#include <cstdio>
#include <cmath>
#include <algorithm>
#include <cstring>
#include <string>
#include <cctype>
#include <cstdlib>
#include <map>
#include <ctime>
#include <vector>
#include <queue>
#define debuglog(x) cerr<<"\tdebug:"<<#x<<endl
#define debug(x) cerr<<"\tdebug:"<<#x<<"="<<(x)<<endl
#define debugg(x,y) cerr<<"\tdebug:"<<(x)<<":"<<#y<<"="<<(y)<<endl
#define debugzu(x,a,b) 	cerr<<"\tdebug:"<<#x<<"=\n\t";for(int i=a;i<b;i++)cerr<<x[i]<<" ";fprintf(stderr,"\n");
#define debugerzu(x,a,b,c,d) 	cerr<<"\tdebug:"<<#x<<"=\n\t";for(int i=a;i<b;i++,fprintf(stderr,"\n\t"))for(int j=c;j<d;j++)cerr<<x[i][j]<<" ";fprintf(stderr,"\n");
#define R register
#define L long
#define LL long long
#define I inline
#define U unsigned
#define START clock_t __start=clock();
#define STOP fprintf(stderr,"\n\nUse Time:%fs\n",((double)(clock()-__start)/CLOCKS_PER_SEC));
using namespace std;
I LL read()
{
	R LL x;R bool f;R char c;
	for (f=0; (c=getchar())<'0'||c>'9'; f=c=='-');
	for (x=c-'0'; (c=getchar())>='0'&&c<='9'; x=(x<<3)+(x<<1)+c-'0');
	return f?-x:x;
}
#define MAXN 1011
int data[MAXN][MAXN];



int main()
{
 	freopen("7-4.in","r",stdin);
// 	freopen("7-4.out","w",stdout);
    int n=read();
    for(int i=0;i<n;++i)
        for(int j=0;j<n;++j)
            data[i][j]=read();
    int x1,x2,y1,y2;
//从左上枚举
    for(x1=0;x1<n;++x1)
        for(y1=0;y1<n;++y1)
            if(data[x1][y1]==0)
                goto e1;
e1:;
//从右下枚举
    for(x2=n-1;x2>=0;--x2)
        for(y2=n-1;y2>=0;--y2)
            if(!data[x2][y2])
                goto e2;
e2:;
    cout<<(x2-x1-1)*(y2-y1-1);
    
	fclose(stdin);
	fclose(stdout);
 	return 0;
}