#include <iostream>
#include <cstdio>
#include <cmath>
#include <algorithm>
#include <cstring>
#include <string>
#include <cctype>
#include <cstdlib>
#include <map>
#include <ctime>
#include <vector>
#include <queue>
#define debuglog(x) cerr<<"\tdebug:"<<#x<<endl
#define debug(x) cerr<<"\tdebug:"<<#x<<"="<<(x)<<endl
#define debugg(x,y) cerr<<"\tdebug:"<<(x)<<":"<<#y<<"="<<(y)<<endl
#define debugzu(x,a,b) 	cerr<<"\tdebug:"<<#x<<"=\n\t";for(int i=a;i<b;i++)cerr<<x[i]<<" ";fprintf(stderr,"\n");
#define debugerzu(x,a,b,c,d) 	cerr<<"\tdebug:"<<#x<<"=\n\t";for(int i=a;i<b;i++,fprintf(stderr,"\n\t"))for(int j=c;j<d;j++)cerr<<x[i][j]<<" ";fprintf(stderr,"\n");
#define R register
#define L long
#define LL long long
#define I inline
#define U unsigned
#define START clock_t __start=clock();
#define STOP fprintf(stderr,"\n\nUse Time:%fs\n",((double)(clock()-__start)/CLOCKS_PER_SEC));
using namespace std;
I LL read()
{
	R LL x;R bool f;R char c;
	for (f=0; (c=getchar())<'0'||c>'9'; f=c=='-');
	for (x=c-'0'; (c=getchar())>='0'&&c<='9'; x=(x<<3)+(x<<1)+c-'0');
	return f?-x:x;
}
typedef struct
{
    char name[50];
    char id  [50];
    int h,m;
    int day;
    int shenti;
    int cnt;
}person;
map   <string,person>all;
map   <string,int>   shenti_notok_map;
bool cmp1(person a,person b)
{
    int at=a.h*60+a.m,bt=b.h*60+b.m;
    if(at==bt)
        return a.cnt<b.cnt;
    return at<bt;
}
bool check_id(char s[])
{
    int i=0;
   for(i=0;s[i];++i)
       if(s[i]<'0'||s[i]>'9')
           return 0;
    return i==18;
}
void person_copy(person *a,person *b)
{
    strcpy(a->name,b->name);
    strcpy(a->id,b->id);
    a->h=b->h;
    a->m=b->m;
    a->day=b->day;
    a->shenti=b->shenti;
    a->cnt=b->cnt;
}
person today[100000];
person not_ok[100000];
int not_ok_cnt;


int main()
{
 	freopen("7-5.in","r",stdin);
// 	freopen("7-5.out","w",stdout);
    for(int day=0,D=read(),p=read();day<D;++day)
    {
        int t=read(),s=read();
        int cnt=0;
        while(t--)
        {
            scanf("%s %s %d %d:%d",today[cnt].name,today[cnt].id,&today[cnt].shenti,&today[cnt].h,&today[cnt].m);
            today[cnt].cnt=cnt;
            if(check_id(today[cnt].id))
            {
                if(today[cnt].shenti&&!shenti_notok_map[today[cnt].id])
                {                    
                    shenti_notok_map[today[cnt].id]=1;
                    person_copy(&not_ok[not_ok_cnt],&today[cnt]);
                    ++not_ok_cnt;
                }
                if(all.count(today[cnt].id)==0)
                {
                    all[today[cnt].id]=today[cnt];
                    all[today[cnt].id].day=-10000;                    
                }
                ++cnt;
            }
        }
        sort(today,today+cnt,cmp1);
        for(int j=0;j<cnt;++j)
            if(s&&day>=all[today[j].id].day+p+1)
            {
                --s;
                all[today[j].id].day=day;
                printf("%s %s\n",today[j].name,today[j].id);
            }
    }
    for(int i=0;i<not_ok_cnt;++i)
        printf("%s %s\n",not_ok[i].name,not_ok[i].id);
    
	fclose(stdin);
	fclose(stdout);
 	return 0;
}