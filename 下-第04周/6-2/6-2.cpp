#include <stdio.h>
#define MAXN 20

void strmcpy( char *t, int m, char *s );
void ReadString( char s[] ); /* 由裁判实现，略去不表 */

int main()
{
    char t[MAXN], s[MAXN];
    int m;

    scanf("%d\n", &m);
    ReadString(t);
    strmcpy( t, m, s );
    printf("%s\n", s);

    return 0;
}

/* 你的代码将被嵌在这里 */
#include <string.h>

void strmcpy( char *t, int m, char *s )
{
    int n=strlen(t);
    int cnt=0;
    for(int i=m-1;i<n;++i)
        s[cnt]=t[i],++cnt;
    s[cnt]=0;
}