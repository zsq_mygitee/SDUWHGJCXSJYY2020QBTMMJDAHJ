#include <stdio.h>
#define MAXS 30

char *search(char *s, char *t);
void ReadString( char s[] ); /* 裁判提供，细节不表 */

int main()
{
    char s[MAXS]="The C Programming Language", t[MAXS]="ram", *pos;

    pos = search(s, t);
    if ( pos != NULL )
        printf("%d\n", pos - s);
    else
        printf("-1\n");

    return 0;
}

/* 你的代码将被嵌在这里 */
char *search(char *s, char *t)
{
    for(int i=0;s[i];++i)
    {
        for(int j=0;t[j];++j)
            if(s[i+j]!=t[j])
                goto fail;
        return s+i;
        fail:;
    }
    return NULL;
}