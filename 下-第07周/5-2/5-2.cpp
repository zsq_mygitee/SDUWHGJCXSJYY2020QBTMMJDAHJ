#include<iostream>
using namespace std;
#define OK 1
#define ERROR 0
typedef int ElemType;


typedef struct LNode {
    ElemType data;
    struct LNode *next; 
} LNode, *LinkList; 

void CreateList(LinkList &L, int n) {
    LinkList p;
    int length=0;
    L = new LNode;
    L->next = NULL; 
    while (length<n) {
        p = new LNode; 
        cin >> p->data; 
        
p->next=L->next;

        
L->next=p;

        length++;
    }
} 

void print(LinkList &L)
{
    LinkList p;
    int flag=1;
    p = L->next;
    while (p) {
        if(flag)
            cout << p->data;
        else
            cout << " "<< p->data;
        flag=0;
        p = p->next;
    }
}

int main() {
    LinkList L;
    ElemType e;
    int length; 
    cin >> length;
    CreateList(L, length);    
    print(L);
    return 0;
}