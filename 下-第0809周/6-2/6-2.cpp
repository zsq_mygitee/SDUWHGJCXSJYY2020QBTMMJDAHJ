typedef struct List{
　　int sno;    
　　char sname[10];
　　List *next;
};　
int main(void){
    List *list=NULL,*p;
    int no;
    list=CreateList();
    while(~scanf("%d", &no))
    {
        p=Find(list,no);
        if( p ) printf("%s\n", p->sname);
        else printf("Not Found!\n");
    }
    return 0;
}
/* 请在这里填写答案 */
List * CreateList()
{
    struct List *head=NULL;
    while(1)
    {
        struct List *tmp=(struct List *)malloc(sizeof(struct List));
        scanf("%d%s",&tmp->sno,tmp->sname);
        if(tmp->sno==-1)
            break;
        tmp->next=head;
        head=tmp;
    }
    return head;
}
List * Find(List *head, int no)
{
    for(;head&&head->sno!=no;head=head->next);
    return head;
}