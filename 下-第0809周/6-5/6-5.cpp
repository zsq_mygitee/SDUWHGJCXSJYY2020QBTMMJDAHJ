#include <stdio.h>
#include <stdlib.h>

struct stud_node {
     int    num;
     char   name[20];
     int    score;
     struct stud_node *next;
};

struct stud_node *createlist();
struct stud_node *deletelist( struct stud_node *head, int min_score );

int main()
{
    int min_score;
    struct stud_node *p, *head = NULL;

    head = createlist();
    scanf("%d", &min_score);
    head = deletelist(head, min_score);
    for ( p = head; p != NULL; p = p->next )
        printf("%d %s %d\n", p->num, p->name, p->score);

    return 0;
}

/* 你的代码将被嵌在这里 */
struct stud_node *createlist()
{
    struct stud_node *head=NULL;
    struct stud_node *tail=NULL;
    while(1)
    {
        struct stud_node *tmp=(struct stud_node *)malloc(sizeof(struct stud_node));
        scanf("%d",&tmp->num);
        if(tmp->num==0)
            break;
        scanf("%s%d",tmp->name,&tmp->score);
        tmp->next=NULL;
        if(tail)
            tail->next=tmp,tail=tmp;
        else 
            head=tail=tmp;
    }
    return head;
}
struct stud_node *deletelist( struct stud_node *head, int min_score )
{
    struct stud_node * now=head,*pre=NULL,*tmp=NULL;
    for(;now;)
        if(now->score<min_score)
        {
            if(pre)
                tmp=pre->next=now->next;
            else
                tmp=head=now->next;
            free(now);
            now=tmp;
        }
        else
            pre=now,now=now->next;
    return head;
}

