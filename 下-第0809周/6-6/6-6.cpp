#include <stdio.h>
#include <stdlib.h>

typedef int ElementType;
typedef struct Node *PtrToNode;
struct Node {
    ElementType Data;
    PtrToNode   Next;
};
typedef PtrToNode List;

List Read(); /* 细节在此不表 */
void Print( List L ); /* 细节在此不表；空链表将输出NULL */

List Merge( List L1, List L2 );

int main()
{
    List L1, L2, L;
    L1 = Read();
    L2 = Read();
    L = Merge(L1, L2);
    Print(L);
    Print(L1);
    Print(L2);
    return 0;
}

/* 你的代码将被嵌在这里 */
List Merge( List L1, List L2 )
{
    struct Node* L3=malloc(sizeof(struct Node));
    L3->Next=NULL;
    L3->Data=0;
    struct Node* l1=L1->Next,*l2=L2->Next,*l3=NULL;
    while(l1||l2)
    {
        if(l1&&l2)
        {
            if(l1->Data<l2->Data)
                goto insert_l1;
            goto insert_l2;
        }
        else if(l1)
        {
insert_l1:;
            if(l3)
                l3->Next=l1,l3=l3->Next;
            else
                L3->Next=l3=l1;
            l1=l1->Next;
        }
        else if(l2)
        {
insert_l2:;
            if(l3)
                l3->Next=l2,l3=l3->Next;
            else
                L3->Next=l3=l2;
            l2=l2->Next;
        }
    }
    L1->Next=NULL;
    L2->Next=NULL;
    return L3;
}