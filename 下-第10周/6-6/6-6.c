#include <stdio.h>
#include <string.h>

void FGetStr(char *str, int size, FILE *file);

int main()
{
    FILE *f;
    char a[10], b[10];
    f = fopen("MyStr.txt", "r");
    if (f)
    {
        FGetStr(a, 10, f);
        FGetStr(b, 10, f);
        puts(a);
        puts(b);
        fclose(f);
    }
    return 0;
}

/* 你提交的代码将被嵌在这里 */
void FGetStr(char *str, int size, FILE *file)
{
    int i=0;
    static char last=0;
    if(last!=0)
        str[i]=last,++i;
    while(1)
    {
        char c=getc(file);
        if(i==size-1)
        {
            str[size-1]=0;
            if(c!='\n'&&c!=EOF)
                last=c;
            return;
        }
        if(c==EOF||c=='\n')
        {
            str[i]=0;
            return;
        }
        str[i]=c;
        ++i;
    }
}
